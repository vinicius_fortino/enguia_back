require('dotenv').config();
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
var port = process.env.PORT || 5000;
const router = require('./routes/index');

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Content-Type, Authorization, Access-Control-Allow-Origin');
    if (req.method == 'OPTIONS') return res.sendStatus(200);
    next();
});
app.use(bodyParser.json())
app.use('/api/', router);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})