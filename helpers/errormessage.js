let errormessage = {};

errormessage.generateError = function (error, err) {
    let erro = {
        error: error
    }

    if (err.name === 'SequelizeUniqueConstraintError')
        erro.message = err.errors[0].message;
    else if (err.name === 'SequelizeValidationError')
        erro.message = err.errors[0].path + ' não foi preenchido!';
    else
        erro.message = err.message;

    return erro;
}

module.exports = errormessage;