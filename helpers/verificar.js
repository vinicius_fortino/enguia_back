/*
Funções para verificar a necessidade de troca dos aparelhos.
Retorna o aparelho que necessita de trocas já com os atributos adicionais(atributos da planilha),
Caso não necessite de troca retorna false
*/

function verificaTrocaAr(ar, perfil) {

    //Verificar casos em que a troca não é necessária
    if (ar.tipo === 'Split Inverter')
        return false;

    /* Constantes do calculo */
    //quantidade de uso baseado no perfil
    const quantidadeDeUsoPerfil = {
        verao: {
            economico: {
                semana: 8,
                fds: 8
            },
            moderado: {
                semana: 12,
                fds: 14
            },
            gastador: {
                semana: 14,
                fds: 14
            }
        },
        inverno: {
            economico: {
                semana: 0,
                fds: 0
            },
            moderado: {
                semana: 4,
                fds: 4
            },
            gastador: {
                semana: 12,
                fds: 14
            }
        }
    }

    //eficiencia de ar condicionado COP baseado no tipo e selo
    //todas as sugestões de troca são sugeridas com o selo A
    //TODO não tem E na planilhar então eu coloquei D = E
    let eficienciaSplitA = 3.23;
    const eficienciaCop = {
        Janela: {
            A: 2.91,
            B: 2.78,
            C: 2.67,
            D: 2.55,
            E: 2.55,
            Naosei: 2.55
        },
        Split: {
            A: eficienciaSplitA,
            B: 3.125,
            C: 2.915,
            D: 2.705,
            E: 2.705,
            Naosei: 2.705
        },
        //Eficiencia cop do split inverter é igual a do Split + 10%
        SplitInverter: {
            A: eficienciaSplitA + (eficienciaSplitA * 0.1),
            B: 2.78,
            C: 2.67,
            D: 2.55,
            E: 2.55,
            Naosei: 2.55
        }
    }

    //custos dos novos equipamentos
    const tabelaCustosNovosEquipamentos = {
        9000: {
            Split: 1499,
            SplitInverter: 2248.50
        },
        12000: {
            Split: 1749,
            SplitInverter: 2623.50
        },
        18000: {
            Split: 2649,
            SplitInverter: 3973.50
        },
        20000: {
            Split: 3499,
            SplitInverter: 5248.50
        }
    }

    /* Fim Constantes do calculo */

    //OBJETOS DAS MEMORIAS DE CALCULO DE AR CONDICIONADO PARA VERAO E INVERNO
    let memoriaDeCalculoVerao = {
        potenciaRefrigeracao: 0,
        horasUsoSemanal: 0,
        eficienciaCop: 0,
        total: 0
    }

    let memoriaDeCalculoInverno = {
        potenciaRefrigeracao: 0,
        horasUsoSemanal: 0,
        eficienciaCop: 0,
        total: 0
    }

    //CALCULAR A POTENCIA DO AR
    switch (ar.potencia) {
        case '0-9000':
            memoriaDeCalculoVerao.potenciaRefrigeracao = 9000;
            memoriaDeCalculoInverno.potenciaRefrigeracao = 9000;
            break;
        case '9001-13999':
            memoriaDeCalculoVerao.potenciaRefrigeracao = 12000;
            memoriaDeCalculoInverno.potenciaRefrigeracao = 12000;
            break;
        case '14000-19999':
            memoriaDeCalculoVerao.potenciaRefrigeracao = 18000;
            memoriaDeCalculoInverno.potenciaRefrigeracao = 18000;
            break;
        case '20000-999999':
            memoriaDeCalculoVerao.potenciaRefrigeracao = 20000;
            memoriaDeCalculoInverno.potenciaRefrigeracao = 20000;
            break;
        default:
            memoriaDeCalculoVerao.potenciaRefrigeracao = 0;
            memoriaDeCalculoInverno.potenciaRefrigeracao = 0;
            break;
    }

    /*CALCULO DE TEMPO DE USO DURANTE A SEMANA*/
    memoriaDeCalculoVerao.horasUsoSemanal = ((quantidadeDeUsoPerfil.verao[perfil].semana * 5) + (quantidadeDeUsoPerfil.verao[perfil].fds * 2)) / 7;
    memoriaDeCalculoInverno.horasUsoSemanal = ((quantidadeDeUsoPerfil.inverno[perfil].semana * 5) + (quantidadeDeUsoPerfil.inverno[perfil].fds * 2)) / 7;

    //CALCULO DE EFICIENCIA DO AR
    //formula: =SE(E(E19="Janela";E20="A");P58;SE(E(E19="Janela";E20="B");P59;SE(E(E19="Janela";E20="C");P60;SE(E(E19="Janela";E20="D");P61;SE(E(E19="Janela";E20="não sei");P61;SE(E(E19="Split";E20="A");P64;SE(E(E19="Split";E20="B");P65)))))))
    let tipoAr = ar.tipo.replace(' ', '');
    let eficiencia = eficienciaCop[tipoAr][ar.selo];
    memoriaDeCalculoVerao.eficienciaCop = eficiencia;
    memoriaDeCalculoInverno.eficienciaCop = eficiencia;

    //TOTAL MEMORIA DE CALCULO
    //formula: =((K60*30)*(((K62/K61)*0,00029307107)/K61))
    memoriaDeCalculoVerao.total = ((memoriaDeCalculoVerao.horasUsoSemanal * 30) * (((memoriaDeCalculoVerao.potenciaRefrigeracao / memoriaDeCalculoVerao.eficienciaCop) * 0.00029307107) / memoriaDeCalculoVerao.eficienciaCop));
    memoriaDeCalculoInverno.total = ((memoriaDeCalculoInverno.horasUsoSemanal * 30) * (((memoriaDeCalculoInverno.potenciaRefrigeracao / memoriaDeCalculoInverno.eficienciaCop) * 0.00029307107) / memoriaDeCalculoInverno.eficienciaCop));

    //Consumo base total do equipamento multiplicado pela quantidade
    ar.consumo = (Math.round((((memoriaDeCalculoVerao.total * 3) + (memoriaDeCalculoInverno.total * 3)) / 6) * 100) / 100) * Number(ar.quantidade);

    //Sugestão de trocar de modelo do ar condicionado

    let novoModelo = '';
    if (ar.tipo == 'Janela') {
        novoModelo = 'Split';
    } else {
        novoModelo = 'Split Inverter'
    }

    const novoModelSemEspaco = novoModelo.replace(/ /g, '');

    //a potencia do aparelho sugerido sempre é igual a do aparelho antigo!
    const novaPotencia = memoriaDeCalculoVerao.potenciaRefrigeracao;
    const custoNovoEquipamento = tabelaCustosNovosEquipamentos[novaPotencia][novoModelSemEspaco];

    //Objeto que irá pra página com os dados da sugestão de troca
    //TODO verificar uma maneira melhor de fazer isso
    const novaMemoriaDeCalculoVerao = ((memoriaDeCalculoVerao.horasUsoSemanal * 30) * (((memoriaDeCalculoVerao.potenciaRefrigeracao / eficienciaCop[novoModelSemEspaco].A) * 0.00029307107) / eficienciaCop[novoModelSemEspaco].A));
    const novaMemoriaDeCalculoInverno = ((memoriaDeCalculoInverno.horasUsoSemanal * 30) * (((memoriaDeCalculoInverno.potenciaRefrigeracao / eficienciaCop[novoModelSemEspaco].A) * 0.00029307107) / eficienciaCop[novoModelSemEspaco].A));

    ar.SugestaoTroca = {
        tipo: novoModelo,
        selo: 'A',
        custo: custoNovoEquipamento * Number(ar.quantidade),
        potencia: ar.potencia,
        consumo: (Math.round(((novaMemoriaDeCalculoVerao + novaMemoriaDeCalculoInverno) / 2) * 100) / 100) * Number(ar.quantidade) //<- multiplicando pelo quantidade de ar condicionados
    }
    return ar;
}

/* Método utilizado para sugerir uma troca de lampada, caso a troca seja necessária, é retornado
o objeto SugestaoTroca, caso não seja necessário é retornado false
*/
function verificaTrocaLampada(lampada, perfil) {

    //nestes tipos não trocar de lampada
    if (lampada.tipo === 'Led compacta' || lampada.tipo === 'Led tubular')
        return false;

    //memoria de calculo para lampada
    let memoriaDeCalculoLampada = {
        quantidadeLampadas: lampada.quantidade,
        //horas de uso mensal: economico = 135hrs / moderado  = 270hrs / gastador = 360hrs
        horasUsoMensal: perfil == 'economico' ? 135 : perfil == 'moderado' ? 270 : 360,
        potencia: lampada.potencia / 1000
    }

    //consumo da lampada em kwh baseado na memoraia de calculo
    lampada.consumo = Math.round((memoriaDeCalculoLampada.quantidadeLampadas * memoriaDeCalculoLampada.horasUsoMensal * memoriaDeCalculoLampada.potencia) * 100) / 100;

    //novo tipo de lampada sugerido
    let novoTipo = '';

    //potencia que deverá ter a lampada sugerida
    let novaPotencia = regraDeTrocaPotenciaLampada(lampada.tipo, lampada.potencia);

    switch (lampada.tipo) {
        case 'Incandescente':
            novoTipo = 'Led bulbo'
            break;

        case 'Fluorescente compacta':
            novoTipo = 'Led bulbo'
            break;

        case 'Fluorescente tubular':
            novoTipo = 'Led tubular'
            break;

        case 'Halopar':
            novoTipo = 'Led'
            break;

        default:
            //caso esteja nulo no banco
            novoTipo = 'Led'
            break;
    }

    const custo = novaPotencia[1]; //custo do equipamento

    lampada.SugestaoTroca = {
        tipo: novoTipo,
        custo: custo * Number(lampada.quantidade),
        consumo: Math.round(memoriaDeCalculoLampada.quantidadeLampadas * memoriaDeCalculoLampada.horasUsoMensal * (novaPotencia[0] / 1000) * 100) / 100,
        potencia: novaPotencia[0]
    }
    return lampada;
}

function verificaTrocaChuveiro(chuveiro, perfil, ocupantes, uf) {

    let valorDeCalculoPotencia = 0;
    const potenciaInicial = Number(chuveiro.potencia.split('-')[0]);
    const potenciaFinal = Number(chuveiro.potencia.split('-')[1]);

    if (potenciaInicial <= 2400)
        valorDeCalculoPotencia = 2400;
    else if (potenciaInicial > 7900)
        valorDeCalculoPotencia = 7900
    else
        valorDeCalculoPotencia = Math.floor((potenciaFinal + potenciaInicial) / 2);

    let memoriaCalculo = {
        potencia: valorDeCalculoPotencia,
        tempoBanho: perfil == 'economico' ? 0.17 : perfil == 'moderado' ? 0.25 : 0.4,
        quantidadeBanhos: ocupantes * 2,
        porcentagemPotenciaUtilizada: perfil == 'economico' ? 0.2 : perfil == 'moderado' ? 0.7 : 1,
    }

    //consumo mensal
    //formula: =((H78*H75)*(H76*H77*30))/1000
    let consumo = ((memoriaCalculo.porcentagemPotenciaUtilizada * memoriaCalculo.potencia) * (memoriaCalculo.tempoBanho * memoriaCalculo.quantidadeBanhos * 30)) / 1000;
    //arredondar o valor
    chuveiro.consumo = (Math.round(consumo * 100) / 100) * chuveiro.quantidade
    chuveiro.potencia = valorDeCalculoPotencia;

    //SE(D22="norte";SE(D21>M13;M13;"manter chuveiro");
    //SE(OU(D22="NORDESTE";D22="CENTRO-OESTE");
    //SE(D21>M16;M16;"manter chuveiro");
    //SE(OU(D22="SUL";D22="SUDESTE");SE(D21>M18;M18;"manter chuveiro"))))
    let potenciaEficiente = 0;
    let potenciaIneficiente = 0;

    //verificar qual a região do imóvel
    let regiao = 'Norte';

    if (['AM', 'RR', 'AP', 'PA', 'TO', 'RO', 'AC'].indexOf(uf) > -1)
        regiao = 'Norte'
    else if (['MA', 'PI', 'CE', 'RN', 'PE', 'PB', 'SE', 'AL', 'BA'].indexOf(uf) > -1)
        regiao = 'Nordeste'
    else if (['MT', 'MS', 'GO'].indexOf(uf) > -1)
        regiao = 'Centro-Oeste'
    else if (['RS', 'SC', 'PR'].indexOf(uf) > -1)
        regiao = 'Sul'
    else
        regiao = 'Sudeste'

    switch (regiao) {
        case 'Norte':
            potenciaEficiente = 2400;
            potenciaIneficiente = 4600;
            if (valorDeCalculoPotencia <= potenciaEficiente)
                return false;
            break;

        case 'Nordeste' || 'Centro-Oeste':
            potenciaEficiente = 4600;
            potenciaIneficiente = 6800;
            if (valorDeCalculoPotencia <= potenciaEficiente)
                return false;
            break;

        case 'Sul' || 'Sudeste':
            potenciaEficiente = 6800;
            potenciaIneficiente = 7900;
            if (valorDeCalculoPotencia <= potenciaEficiente)
                return false;
            break;

        default:
            potenciaEficiente = 6800;
            potenciaIneficiente = 7900;
            if (valorDeCalculoPotencia <= potenciaEficiente)
                return false;
            break;
    }

    let texto = `Para sua região é recomendado um chuveiro com potência entre ${potenciaEficiente} e ${potenciaIneficiente}W `;

    let precoChuveiro = {
        2400: 35,
        2950: 35,
        4050: 69,
        5150: 69,
        6250: 190,
        7350: 200,
        1900: 252.40
    }

    //custo do equipamento
    const custo = precoChuveiro[valorDeCalculoPotencia] || 30.00;

    chuveiro.SugestaoTroca = {
        tipo: `${potenciaEficiente}W a ${potenciaIneficiente}W `,
        consumo: Math.round((((potenciaEficiente * memoriaCalculo.tempoBanho * memoriaCalculo.quantidadeBanhos * memoriaCalculo.porcentagemPotenciaUtilizada) * 30) / 1000) * Number(chuveiro.quantidade) * 100) / 100,
        potencia: potenciaEficiente,
        texto: texto,
        custo: custo
    }
    return chuveiro;
}


function verificarTrocaTv(tv, perfil) {

    //Só é necessária a troca da tv em caso do tipo CRT
    let novoTipo = '';
    //tamanho em poleagas que deverá ter a nova tb
    let novoTamanho = 0;
    //custo da nova tv
    let custo = 0;
    if (tv.tipo === 'CRT')
        novoTipo = ['LED', 'LCD', 'PLASMA', 'OLED'];
    else
        return false;

    //memoria de calculo para tv
    let memoriaDeCalculo = {
        horasUsoSemanal: 0,
        modificadorTamanho: 0,
        modificadorIdade: 0,
        potencia: 0
    }
    //idade inicial e idade final da tvAedrons dinheirista
    let idade = [Number(tv.idade.split('-')[0]), Number(tv.idade.split('-')[1])];

    //tamanho inicial e tamanho final
    let tamanho = [Number(tv.tamanho.split('-')[0]), Number(tv.tamanho.split('-')[1])];

    /* Constantes do calculo */
    //quantidade de uso baseado no perfil
    const quantidadeDeUsoPerfil = {
        economico: {
            semana: 8,
            fds: 8
        },
        moderado: {
            semana: 12,
            fds: 14
        },
        gastador: {
            semana: 14,
            fds: 14
        }
    }

    /*CALCULO DE TEMPO DE USO DURANTE A SEMANA*/
    memoriaDeCalculo.horasUsoSemanal = quantidadeDeUsoPerfil[perfil].semana + quantidadeDeUsoPerfil[perfil].fds;

    /*MODIFICADOR DE IDADE */
    //['0-2', '3-5', '6-10', '11-15', '15-999999']
    if (comparaArrays([0, 2], idade))
        memoriaDeCalculo.modificadorIdade = 1;
    else if (comparaArrays([3, 5], idade))
        memoriaDeCalculo.modificadorIdade = 1.1;
    else if (comparaArrays([6, 10], idade))
        memoriaDeCalculo.modificadorIdade = 1.2;
    else if (comparaArrays([11, 15], idade))
        memoriaDeCalculo.modificadorIdade = 1.5;
    else
        memoriaDeCalculo.modificadorIdade = 2;

    /*MODIFICADOR DE TAMANHO */
    /*Comparações também utilizadas para escolher qual tamanho a nova tv terá e o tamanho e a potencia*/
    if (comparaArrays([0, 20], tamanho)) {
        memoriaDeCalculo.modificadorTamanho = 0.2;
        novoTamanho = 20;
        custo = 402.40;
        tv.potencia = 40;
    } else if (comparaArrays([21, 30], tamanho)) {
        memoriaDeCalculo.modificadorTamanho = 0.36;
        novoTamanho = 20;
        custo = 655.20;
        tv.potencia = 70;
    } else if (comparaArrays([31, 40], tamanho)) {
        memoriaDeCalculo.modificadorTamanho = 0.9;
        novoTamanho = 30;
        custo = 759.20;
        tv.potencia = 210;
    } else if (comparaArrays([41, 50], tamanho)) {
        memoriaDeCalculo.modificadorTamanho = 1.3;
        novoTamanho = 40;
        custo = 1120;
        tv.potencia = 315;
    } else {
        memoriaDeCalculo.modificadorTamanho = 1.5;
        novoTamanho = 50;
        custo = 2240;
        tv.potencia = 472.5;
    }

    //potencia STAND BY de uma TV do tipo tubo CRT, dado retirado da planilha. *Somente esse valor é necessária pois só existe troca para tvs do tipo CRT tubo
    const potenciaStandBy = 4.30;
    tv.consumoStandBy = (potenciaStandBy / 1000) * (memoriaDeCalculo.horasUsoSemanal * 4)

    //Consumo da tv
    tv.consumo = (memoriaDeCalculo.horasUsoSemanal * 4) * (tv.potencia * memoriaDeCalculo.modificadorTamanho * memoriaDeCalculo.modificadorIdade) / 1000;
    tv.consumo = (Math.round((tv.consumo + tv.consumoStandBy) * 100) / 100) * tv.quantidade;

    tv.SugestaoTroca = {
        tipo: novoTipo,
        //TODO como a troca pode ser para qualquer tipo de tv, qual valor de potencia utilizar? led? lcd? oled? utilizado aqui a de led
        consumo: (Math.round(((memoriaDeCalculo.horasUsoSemanal * 4 * 124.1) / 1000) * 100) / 100) * tv.quantidade,
        potencia: 124.1,
        texto: 'LED, LCD, PLASMA, OLED',
        custo: custo,
        tamanho: novoTamanho,
    }
    console.log(memoriaDeCalculo);
    return tv;
}

function verificarTrocaGeladeira(geladeira, perfil) {
    //'Uma porta', 'Uma porta frost-free', 'Duplex', 'Duplex frost-free', 'Side by Side'
    //['0-300', '301-400', '401-500', '501-999999']
    //remover espaços
    let tipo = geladeira.tipo.replace(/ /g, '')

    //remover traços
    tipo = tipo.replace('-', '')

    //tamanho (considerado o segundo range, ex 0-300, é considerado 300)
    let tamanho = Number(geladeira.tamanho.split('-')[1]);

    let idade = Number(geladeira.idade.split('-')[1]);

    //TODO algumas geladeiras não tinham custo então repeti, verificar isso
    let consumos = {
        Umaporta: {
            300: {
                consumo: 24,
                preco: 1200
            },
            400: {
                consumo: 27,
                preco: 1500
            },
            500: {
                consumo: 30,
                preco: 2000
            },
            999999: {
                consumo: 35,
                preco: 2000 //
            },
        },
        Umaportafrostfree: {
            300: {
                consumo: 35,
                preco: 1500
            },
            400: {
                consumo: 38,
                preco: 2000
            },
            500: {
                consumo: 42,
                preco: 2000 //
            },
            999999: {
                consumo: 47,
                preco: 2000 //
            },
        },
        Duplex: {
            300: {
                consumo: 40,
                preco: 1200
            },
            400: {
                consumo: 45,
                preco: 1600
            },
            500: {
                consumo: 55,
                preco: 2100
            },
            999999: {
                consumo: 60,
                preco: 3000
            },
        },
        Duplexfrostfree: {
            300: {
                consumo: 45,
                preco: 1200 //
            },
            400: {
                consumo: 50,
                preco: 1800
            },
            500: {
                consumo: 57,
                preco: 2100
            },
            999999: {
                consumo: 65,
                preco: 2500
            },
        },
        SidebySide: {
            300: {
                consumo: 60, //
                preco: 6700 //
            },
            400: {
                consumo: 60, //
                preco: 6700 //
            },
            500: {
                consumo: 60,
                preco: 6700
            },
            999999: {
                consumo: 75,
                preco: 8000
            },
        }
    }

    //calcular o consumo atual da geladeira baseado na idade
    //['0-5', '6-10', '11-999999']
    if (idade === 10) {
        geladeira.consumo = Math.round((consumos[tipo][tamanho].consumo + (consumos[tipo][tamanho].consumo * 0.15)) * 100) / 100;
    } else if (idade === 999999) {
        geladeira.consumo = Math.round((consumos[tipo][tamanho].consumo + (consumos[tipo][tamanho].consumo * 0.20)) * 100) / 100;
    } else {
        geladeira.consumo = consumos[tipo][tamanho].consumo;
    }

    //calculo das potencias da geladeira. //TODO conferir esse calculo -> (1000 * x)/horasmensais = potencia
    geladeira.potencia = arredondar((1000 * geladeira.consumo) / 730);

    //primeira regra de troca: trocar por mesmo modelo, geladeira nova
    //=SE(M5=Q7;Q6;SE(M5=Q6;Q5;SE(M5=Q5;Q4;"Já é a menor capacidade, ver 3")))
    if (idade >= 10) {
        geladeira.SugestaoTroca = {
            tipo: 'Geladeira nova do mesmo modelo',
            consumo: consumos[tipo][tamanho].consumo,
            custo: consumos[tipo][tamanho].preco,
        }
        //sugerir a troca por um tamanho menor
    } else if ((tamanho > 300)) { //se tamanho já é 300, então passar para o proximo criterio
        let novoTipo = '';
        let novoTamanho = 0;
        if (tamanho === 400) {
            novoTipo = '300 litros';
            novoTamanho = 300;
        } else if (tamanho === 500) {
            novoTipo = '300 a 400 litros';
            novoTamanho = 400;
        } else {
            novoTipo = '404 a 500 litros';
            novoTamanho = 500;
        }

        geladeira.SugestaoTroca = {
            tipo: novoTipo,
            consumo: consumos[tipo][novoTamanho].consumo,
            custo: consumos[tipo][novoTamanho].preco,
        }

    } else {
        //trocar por um modelo de menor consumo
        let retornar = false;
        let novoModelo = '';
        let novoTipoKey = '';
        switch (tipo) {
            case 'Umaporta':
                //nesse caso não sugerir troca
                retornar = true;
                break;
            case 'Umaportafrostfree':
                novoModelo = 'Uma porta';
                novoTipoKey = 'Umaporta'
                break;
            case 'Duplex':
                novoModelo = 'Uma porta frost free';
                novoTipoKey = 'Umaportafrostfree'
                break;
            case 'Duplexfrostfree':
                novoModelo = 'Duplex';
                novoTipoKey = 'Duplex'
                break;
            case 'SidebySide':
                novoModelo = 'Duplex frost free'
                novoTipoKey = 'Duplexfrostfree'
                break;
        }
        if (retornar)
            return false;

        geladeira.SugestaoTroca = {
            tipo: novoModelo,
            consumo: consumos[novoTipoKey][tamanho].consumo,
            custo: consumos[novoTipoKey][tamanho].preco,
        }
    }

    //calculo da NOVA geladeira. //TODO conferir esse calculo -> (1000 * x)/horasmensais = potencia
    geladeira.SugestaoTroca.potencia = arredondar((1000 * geladeira.SugestaoTroca.consumo) / 730);
    return geladeira;
}

function verificarTrocaOutros(outro, perfil) {
    return false;
}

//FUNCOES AUXILIARES
function arredondar(numero) {
    return Math.round(numero * 100) / 100;
}

//Retorna um array de 2 posições, a primeira é a nova potencia, e a segunda é o valor da nova lampada
function regraDeTrocaPotenciaLampada(tipo, potencia) {
    //remover os espacos do tipo
    tipo = tipo.replace(/ /g, '')

    //converte a potencia de string para Number, para que comparações com === funcionem. 
    potencia = Number(potencia);

    if (tipo === 'Incandescente') {
        if (potencia > 0 && potencia <= 25) {
            return [5, 7.5];
        } else if (potencia > 25 && potencia <= 40) {
            return [7, 12.9];
        } else {
            return [10, 14.7];
        }
    } else if (tipo === 'Fluorescentetubular') {
        if (potencia === 16 || potencia === 18) {
            return [9, 40.90];
        } else if (potencia === 32 || potencia === 35) {
            return [10, 40.90];
        } else if (potencia === 20) {
            return [16, 49.90];
        } else if (potencia === 40) {
            return [18, 59.90];
        }
    } else if (tipo === 'Fluorescentecompacta') {
        if (potencia > 0 && potencia <= 3) {
            return [1, 12.00];
        } else if (potencia > 3 && potencia <= 7) {
            return [3, 12.00];
        } else if (potencia > 7 && potencia <= 11) {
            return [5, 15.99];
        } else if (potencia > 11 && potencia <= 15) {
            return [7, 19.90];
        } else if (potencia > 15 && potencia <= 19) {
            return [9, 19.90];
        } else if (potencia > 19 && potencia <= 25) {
            return [12, 24.90];
        } else if (potencia > 25 && potencia <= 31) {
            return [16, 46.90];
        } else if (potencia > 31 && potencia <= 36) {
            return [18, 48.90];
        }
        //TODO caso seja Halopar, estou sempre retornando uma lampada led de 18, verificar essa informação
    } else if (tipo === 'Halopar') {
        return [18, 48.90];
    }
    return [1, 1];
}

function comparaArrays(arr1, arr2) {
    if (arr1.length !== arr2.length)
        return false;
    for (var i = arr1.length; i--;) {
        if (arr1[i] !== arr2[i])
            return false;
    }
    return true;
}

module.exports = {
    verificaTrocaAr,
    verificaTrocaChuveiro,
    verificaTrocaLampada,
    verificarTrocaGeladeira,
    verificarTrocaOutros,
    verificarTrocaTv,
}