
const Pool = require('pg').Pool;
console.log("DATABASE_URL", process.env.DATABASE_URL);
const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
})

const geAmbientes = (request, response) => {
    pool.query('SELECT * FROM "Ambiente" ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

module.exports = {
    geAmbientes,
}