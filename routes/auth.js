const express = require('express');
const router = express.Router();
const db = require('../models');
const Op = db.Sequelize.Op;
const request = require('request-promise');
const CLIENT_ID = require('../config/google_client_id');
const {
  OAuth2Client
} = require('google-auth-library');
const client = new OAuth2Client(CLIENT_ID);
const facebook = require('../config/facebook');
const bcrypt = require('bcrypt-nodejs');
const secret = require('../config/secret')[process.env.NODE_ENV];
const jwt = require('jsonwebtoken');
const errormessage = require('../helpers/errormessage');

router.post('/login', async (req, res, next) => {
  try {

    let login = req.body;

    let Usuario;
    let tokenJWT;

    console.log("======= req", req);
    console.log("======= login", login);
    if (login.senha) {
      Usuario = await db.Usuario.findOne({
        where: {
          email: login.email
        },
        attributes: ['id', 'email', 'senha', 'nome', 'etapacadastro']
      })

      console.log("== Usuario", Usuario);

      if (!Usuario)
        throw Error('Usuário não encontrado');

      console.log(Usuario.get({
        plain: true
      }))

      let result = await bcrypt.compare(login.senha, Usuario.senha);

      if (!result)
        throw Error('Usuário não existe ou senha incorreta');

      tokenJWT = jwt.sign({
        email: Usuario.email
      }, secret, {
          issuer: 'Enguia'
        });
    } else if (login.facebookid && login.accessToken) {

      let url = `https://graph.facebook.com/debug_token?input_token=${login.accessToken}&access_token=${facebook.app_id}|${facebook.secret}`;

      let response = await request.get(url);

      if (response.data && !response.data.is_valid) {
        throw Error('Token de acesso inválido');
      }

      if (login.email) {
        Usuario = await db.Usuario.findOne({
          where: {
            [Op.or]: [{
              email: login.email
            }, {
              facebookid: login.facebookid
            }]
          },
          attributes: ['id', 'facebookid', 'email', 'nome', 'etapacadastro']
        })

        if (!Usuario)
          throw Error('Usuário não encontrado');

        if (!Usuario.facebookid)
          Usuario.facebookid = login.facebookid;

        tokenJWT = jwt.sign({
          facebookid: Usuario.facebookid
        }, secret, {
            issuer: 'Enguia'
          });
      }
    } else if (login.googleid && login.idToken) {
      await client.verifyIdToken({
        idToken: req.body.idToken,
        audience: CLIENT_ID
      });

      if (login.email) {
        Usuario = await db.Usuario.findOne({
          where: {
            [Op.or]: [{
              email: login.email
            }, {
              googleid: login.googleid
            }]
          },
          attributes: ['id', 'googleid', 'email', 'nome', 'etapacadastro']
        })

        if (!Usuario)
          throw Error('Usuário não encontrado');

        if (!Usuario.googleid)
          Usuario.googleid = login.googleid;

        tokenJWT = jwt.sign({
          googleid: Usuario.googleid
        }, secret, {
            issuer: 'Enguia'
          });
      }
    } else {
      throw Error('Dados insuficientes para realizar o login');
    }

    let sessao = await db.Sessao.create({
      tokensessao: tokenJWT,
      UsuarioId: Usuario.id
    })

    await Usuario.save();

    Usuario.senha = undefined;
    res.status(200).send({
      tokensessao: tokenJWT,
      Usuario: Usuario
    })
  } catch (err) {
    let erro = errormessage.generateError('Erro ao logar usuário', err);
    res.status(400).send(erro)
  }
});

router.delete('/logout', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let tokensessao = req.sessao.tokensessao;
    await db.Sessao.destroy({
      where: {
        tokensessao: tokensessao
      }
    });

    res.status(200).send({
      message: 'Sessão destruida',
    })
  } catch (err) {
    let erro = errormessage.generateError('Erro ao deslogar usuário', err);
    res.status(400).send(erro)
  }
});

module.exports = router;
