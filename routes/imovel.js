const express = require('express');
const router = express.Router();
const geraErro = require('../helpers/generate-error');
const db = require('../models');
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt-nodejs');
const errormessage = require('../helpers/errormessage');

router.post('/create', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let body = req.body;
    let UsuarioId;
    let Imovelcomercial;
    let Imovelresidencial;
    //Imovel residencial
    if (body.Imovelresidencial) {
      UsuarioId = body.Imovelresidencial.UsuarioId;
      Imovelresidencial = await db.Imovelresidencial.create(body.Imovelresidencial);
      let ambientes = [];
      for (let i = 1; i <= body.Imovelresidencial.numeroquartos; i++) {
        ambientes.push({
          nome: `Quarto ${i}`,
          tipo: `Quarto`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.numerosalas; i++) {
        ambientes.push({
          nome: `Sala ${i}`,
          tipo: `Sala`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.numerocozinhas; i++) {
        ambientes.push({
          nome: `Cozinha ${i}`,
          tipo: `Cozinha`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.numerobanheiros; i++) {
        ambientes.push({
          nome: `Banheiro ${i}`,
          tipo: `Banheiro`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.outrosambientes; i++) {
        ambientes.push({
          nome: `Ambiente ${i}`,
          tipo: `Outro`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      await db.Ambiente.bulkCreate(ambientes);
    }
    //Imovel Comercial
    if (body.Imovelcomercial) {
      UsuarioId = body.Imovelcomercial.UsuarioId;
      Imovelcomercial = await db.Imovelcomercial.create(body.Imovelcomercial);
      let ambientes = [];
      for (let i = 1; i <= body.Imovelcomercial.numeroambientes; i++) {
        ambientes.push({
          nome: `Ambiente ${i}`,
          tipo: `Outro`,
          ImovelcomercialId: Imovelcomercial.id
        });
      }
      for (let i = 1; i <= body.Imovelcomercial.numerobanheiros; i++) {
        ambientes.push({
          nome: `Banheiro ${i}`,
          tipo: `Banheiro`,
          ImovelcomercialId: Imovelcomercial.id
        });
      }
      await db.Ambiente.bulkCreate(ambientes);
    }

    //repliquei essa rota abaixo porque quando é adicionado um imovel posteriormente, esse update abaixo não deve ser executado
    //devido a urgencia do projeto, repliquei para que não tivesse que alterar código já pronto no front end
    await db.Usuario.update({
      etapacadastro: 'equipamentos'
    }, {
      where: {
        id: UsuarioId
      }
    });

    res.status(200).send({
      message: 'Imovel cadastrado',
      Imovelcomercial: Imovelcomercial,
      Imovelresidencial: Imovelresidencial
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao cadastrar o imovel', err);
    res.status(400).send(erro)
  }
})

//TODO remover rota depois
router.post('/adicionar', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let body = req.body;
    let UsuarioId = req.sessao.Usuario.id;
    let Imovelcomercial;
    let Imovelresidencial;
    //Imovel residencial
    if (body.Imovelresidencial) {
      body.Imovelresidencial.UsuarioId = UsuarioId;
      Imovelresidencial = await db.Imovelresidencial.create(body.Imovelresidencial);
      let ambientes = [];
      for (let i = 1; i <= body.Imovelresidencial.numeroquartos; i++) {
        ambientes.push({
          nome: `Quarto ${i}`,
          tipo: `Quarto`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.numerosalas; i++) {
        ambientes.push({
          nome: `Sala ${i}`,
          tipo: `Sala`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.numerocozinhas; i++) {
        ambientes.push({
          nome: `Cozinha ${i}`,
          tipo: `Cozinha`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.numerobanheiros; i++) {
        ambientes.push({
          nome: `Banheiro ${i}`,
          tipo: `Banheiro`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      for (let i = 1; i <= body.Imovelresidencial.outrosambientes; i++) {
        ambientes.push({
          nome: `Ambiente ${i}`,
          tipo: `Outro`,
          ImovelresidencialId: Imovelresidencial.id
        });
      }
      await db.Ambiente.bulkCreate(ambientes);
    }
    //Imovel Comercial
    if (body.Imovelcomercial) {
      body.Imovelcomercial.UsuarioId = UsuarioId;
      Imovelcomercial = await db.Imovelcomercial.create(body.Imovelcomercial);
      let ambientes = [];
      for (let i = 1; i <= body.Imovelcomercial.numeroambientes; i++) {
        ambientes.push({
          nome: `Ambiente ${i}`,
          tipo: `Outro`,
          ImovelcomercialId: Imovelcomercial.id
        });
      }
      for (let i = 1; i <= body.Imovelcomercial.numerobanheiros; i++) {
        ambientes.push({
          nome: `Banheiro ${i}`,
          tipo: `Banheiro`,
          ImovelcomercialId: Imovelcomercial.id
        });
      }
      await db.Ambiente.bulkCreate(ambientes);
    }

    res.status(200).send({
      message: 'Imovel cadastrado',
      Imovelcomercial: Imovelcomercial,
      Imovelresidencial: Imovelresidencial
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao cadastrar o imovel', err);
    res.status(400).send(erro)
  }
})

router.get('/getList', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let imoveisComerciais = await db.Imovelcomercial.findAll({
      where: {
        UsuarioId: req.sessao.Usuario.id
      },
      attributes: ['id', 'nome']
    });
    let imoveisResidenciais = await db.Imovelresidencial.findAll({
      where: {
        UsuarioId: req.sessao.Usuario.id
      },
      attributes: ['id', 'nome']
    });

    res.status(200).send({
      message: 'Imóves obtidos com sucesso',
      imoveisComerciais: imoveisComerciais,
      imoveisResidenciais: imoveisResidenciais
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao consultar imóveis', err);
    res.status(400).send(erro)
  }
})

router.post('/getById', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let id = req.body.id;
    let tipo = req.body.tipo;

    let imovel = await db[tipo].findById(id, {
      include:[{
        model: db.Cidade,
        required: true,
        include:[{
          model: db.Estado,
          required: true
        }]
      }]
    });

    res.status(200).send({
      message: 'Imóvel obtido com sucesso',
      imovel: imovel
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao consultar imóvel', err);
    res.status(400).send(erro)
  }
})

router.post('/update/:tipo', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    const tipo = req.params.tipo;
    let imovel = req.body;
    
    await db[tipo].update(imovel, {
      where: {
        id: imovel.id
      }
    });

    res.status(200).send({
      message: 'Alterações salvas',
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao atualizar dados do imóvel', err);
    res.status(400).send(erro)
  }
})

module.exports = router;
