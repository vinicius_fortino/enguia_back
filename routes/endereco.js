const express = require('express');
const router = express.Router();
const geraErro = require('../helpers/generate-error');
const db = require('../models');
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt-nodejs');
const errormessage = require('../helpers/errormessage');

router.get('/cidades/:uf', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);
    
    let uf = req.params.uf;
    let Cidades = await db.Cidade.findAll({
      include: [{
        model: db.Estado,
        required: true,
        where: {
          uf: uf
        }
      }]
    });

    res.status(200).send({
      message: 'Cidades de ' + uf + ' obtidas',
      Cidades: Cidades
    })
  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao obter cidades', err);
    res.status(400).send(erro)
  }
})

module.exports = router;
