const express = require('express');
const router = express.Router();
const geraErro = require('../helpers/generate-error');
const db = require('../models');
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt-nodejs');
const errormessage = require('../helpers/errormessage');

//TODO verificar se o token do ambiente é do usuário.
router.post('/', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let usuario = await db.Usuario.findById(req.sessao.Usuario.id, {
      attributes: ['id', 'etapacadastro']
    });

    let ambiente = req.body;
    let tabela = req.body.tabela;

    for (let i = 0; i < ambiente.equipamentos.length; i++) {
      let equipamento = ambiente.equipamentos[i];
      if (equipamento.operacao) {
        if (equipamento.operacao === 'destroy') {
          //destroy
          await db[tabela].destroy({
            where: {
              id: equipamento.id
            }
          });
        } else {
          //create e update
          await db[tabela][equipamento.operacao](equipamento);
        }
      }
    }

    if (usuario.etapacadastro === 'equipamentos') {
      await db.Usuario.update({
        etapacadastro: 'diagnostico'
      }, {
        where: {
          id: usuario.id
        }
      });
    }


    res.status(200).send({
      message: 'Equipamento cadastrado'
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao cadastrar o equipamento', err);
    res.status(400).send(erro)
  }
})

router.post('/getequipamentos', async (req, res, next) => {
  try {
    if (!req.sessao)
      return next(req.sessaoNaoAutorizada);

    let equipamentos = await db.Ambiente.findOne({
      where: {
        id: req.body.AmbienteId
      },
      include: [{
        model: db[req.body.tabela],
        required: false
      }]
    });

    res.status(200).send({
      message: 'Equipamento cadastrado',
      Equipamentos: equipamentos
    })

  } catch (err) {
    console.log(err);
    let erro = errormessage.generateError('Erro ao obter os equipamentos do ambiente', err);
    res.status(400).send(erro)
  }
})

module.exports = router;
