const express = require('express');
const router = express.Router();
const jwtmid = require('../middlewares/auth')
const auth = require('./auth');
const usuario = require('./usuario');
const imovel = require('./imovel');
const endereco = require('./endereco');
const equipamentos = require('./equipamentos');

router.use(jwtmid);
router.use('/auth', auth);
router.use('/usuario', usuario);
router.use('/imovel', imovel);
router.use('/endereco', endereco);
router.use('/equipamento', equipamentos);

// catch 404 and forward to error handler
router.use(function (req, res, next) {
    res.status(404).json({
        error: 'Página não encontrada',
        message: 'Página não encontrada'
    });
});

router.use(function (err, req, res, next) {
    let message = err.message;
    let error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500).json({
        error: error,
        message: message
    });
});
module.exports = router;
