module.exports = function (sequelize, DataTypes) {
  var Imovelresidencial = sequelize.define('Imovelresidencial', {
    nome: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'Residencial'
    },
    tipo: {
      type: DataTypes.ENUM(['Casa', 'Apartamento']),
      allowNull: false
    },
    cep: {
      type: DataTypes.STRING,
      allowNull: true
    },
    area: {
      type: DataTypes.ENUM(['0-50', '51-100', '101-150', '151-250', '251-450', '451-999999']),
      allowNull: false
    },
    numeroquartos: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numerosalas: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numerocozinhas: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numerobanheiros: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    outrosambientes: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    possuiareaexterna: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    ocupantes: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });
  Imovelresidencial.associate = function (models) {
    Imovelresidencial.belongsTo(models.Cidade);
    Imovelresidencial.belongsTo(models.Usuario);
    Imovelresidencial.hasMany(models.Ambiente, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: true
      }
    });
  };
  return Imovelresidencial;
}
