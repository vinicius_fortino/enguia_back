module.exports = function (sequelize, DataTypes) {
  let Ambiente = sequelize.define('Ambiente', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false
    },
    tipo: {
      type: DataTypes.ENUM(['Cozinha', 'Sala', 'Banheiro', 'Quarto', 'Outro']),
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  Ambiente.associate = function (models) {
    Ambiente.belongsTo(models.Imovelcomercial)
    Ambiente.belongsTo(models.Imovelresidencial)
    Ambiente.hasMany(models.Lampada, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: true
      }
    })
    Ambiente.hasMany(models.Chuveiro, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: true
        }
      }),
      Ambiente.hasMany(models.Televisao, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: true
        }
      })
    Ambiente.hasMany(models.Arcondicionado, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: true
      }
    })
    Ambiente.hasMany(models.Geladeira, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: true
      }
    })
    Ambiente.hasMany(models.Outros, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: true
      }
    })
  }
  return Ambiente;
};
