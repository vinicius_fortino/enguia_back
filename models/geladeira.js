module.exports = function (sequelize, DataTypes) {
  let Geladeira = sequelize.define('Geladeira', {
    tipo: {
      type: DataTypes.ENUM(['Uma porta', 'Uma porta frost-free', 'Duplex', 'Duplex frost-free', 'Side by Side']),
      allowNull: false
    },
    tamanho: {
      type: DataTypes.ENUM(['0-300', '301-400', '401-500', '501-999999']),
      allowNull: false
    },
    idade: {
      type: DataTypes.ENUM(['0-5', '6-10', '11-999999']),
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  Geladeira.associate = function (models) {
    Geladeira.belongsTo(models.Ambiente)
  }
  return Geladeira;
};
