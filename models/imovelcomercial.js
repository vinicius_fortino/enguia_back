module.exports = function (sequelize, DataTypes) {
  var Imovelcomercial = sequelize.define('Imovelcomercial', {
    nome: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'Comercial'
    },
    tipo: {
      type: DataTypes.ENUM(['Salao', 'Sala', 'Casa']),
      allowNull: false
    },
    cep: {
      type: DataTypes.STRING,
      allowNull: true
    },
    area: {
      type: DataTypes.ENUM(['0-50', '51-100', '101-150', '151-250', '251-450', '451-999999']),
      allowNull: false
    },
    numeroambientes: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numerobanheiros: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    possuicopa: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    possuiareaexterna: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    tempofuncionamento: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ocupantes: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });
  Imovelcomercial.associate = function (models) {
    Imovelcomercial.belongsTo(models.Cidade);
    Imovelcomercial.belongsTo(models.Usuario);
    Imovelcomercial.hasMany(models.Ambiente, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: true
      }
    });
  };
  return Imovelcomercial;
}
