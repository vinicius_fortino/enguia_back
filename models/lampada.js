module.exports = function (sequelize, DataTypes) {
  let Lampada = sequelize.define('Lampada', {
    tipo: {
      type: DataTypes.ENUM(['Fluorescente compacta', 'Fluorescente tubular', 'Halopar', 'Incandescente', 'Led compacta', 'Led tubular']),
      allowNull: false
    },
    quantidade: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    potencia: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  Lampada.associate = function (models) {
    Lampada.belongsTo(models.Ambiente)
  }
  return Lampada;
};