module.exports = function (sequelize, DataTypes) {
    let Arcondicionado = sequelize.define('Arcondicionado', {
      tipo: {
        type: DataTypes.ENUM(['Split', 'Janela', 'Split Inverter']),
        allowNull: false
      },
      quantidade: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      selo: {
        type: DataTypes.ENUM(['A', 'B', 'C', 'D', 'E', 'Nao sei']),
        allowNull: false
      },
      potencia: {
        type: DataTypes.ENUM(['0-9000', '9001-13999', '14000-19999', '20000-999999']),
        allowNull: false
      },
    }, {
      freezeTableName: true
    });
  
    Arcondicionado.associate = function (models) {
      Arcondicionado.belongsTo(models.Ambiente)
    }
    return Arcondicionado;
  };
  