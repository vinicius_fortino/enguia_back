module.exports = function (sequelize, DataTypes) {
  let Sessao = sequelize.define('Sessao', {
    tokensessao: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
      freezeTableName: true
    });

  Sessao.associate = function(models){
    Sessao.belongsTo(models.Usuario)
  }
  return Sessao;
};
