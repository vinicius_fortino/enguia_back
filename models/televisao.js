module.exports = function (sequelize, DataTypes) {
  let Televisao = sequelize.define('Televisao', {
    tipo: {
      type: DataTypes.ENUM(['Plasma', 'LED', 'OLED', 'LCD', 'CRT']),
      allowNull: false
    },
    quantidade: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    idade: {
      type: DataTypes.ENUM(['0-2', '3-5', '6-10', '11-15', '15-999999']),
      allowNull: false
    },
    tamanho: {
      type: DataTypes.ENUM(['0-20', '21-30', '31-40', '41-50', '51-999999']),
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  Televisao.associate = function (models) {
    Televisao.belongsTo(models.Ambiente)
  }
  return Televisao;
};