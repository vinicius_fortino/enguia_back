module.exports = function (sequelize, DataTypes) {
  let Estado = sequelize.define('Estado', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false
    },
    uf: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
      freezeTableName: true,
      timestamps: false
    });
  Estado.associate = function (models) {
    Estado.hasMany(models.Cidade, {
      foreignKey: {
        allowNull: false
      }
    });
  }
  return Estado;
};
