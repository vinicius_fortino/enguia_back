module.exports = function (sequelize, DataTypes) {
  let Cidade = sequelize.define('Cidade', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
      freezeTableName: true,
      timestamps: false
    });
  Cidade.associate = function (models) {
    Cidade.hasMany(models.Endereco, {
      foreignKey: {
        allowNull: false
      }
    });
    Cidade.belongsTo(models.Estado);
  }
  return Cidade;
};
