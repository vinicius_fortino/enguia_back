module.exports = function (sequelize, DataTypes) {
  const Concessionaria = sequelize.define('Concessionaria', {
    distribuidora: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Nome da concessionaria deve ser preenchido'
      }
    },
    tarifa: {
      type: DataTypes.DOUBLE,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });
  Concessionaria.associate = function (models) {
    // associations can be defined here
  };
  return Concessionaria;
}
