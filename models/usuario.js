module.exports = function (sequelize, DataTypes) {
  var Usuario = sequelize.define('Usuario', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sobrenome: {
      type: DataTypes.STRING,
      allowNull: true
    },
    senha: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    facebookid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    googleid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    etapacadastro: {
      type: DataTypes.ENUM(['perfildeconsumo', 'imovel', 'equipamentos', 'diagnostico']),
      defaultValue: 'perfildeconsumo',
      allowNull: false
    },
    perfilconsumo: {
      type: DataTypes.ENUM(['economico', 'moderado', 'gastador', 'semperfil']),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Email já cadastrado em nossa base'
      },
      validate: {
        isEmail: {
          msg: 'Email inválido'
        }
      },
    },
    sexo: {
      type: DataTypes.ENUM(['Masculino', 'Feminino', 'Outro']),
      allowNull: false
    },
    nascimento: {
      type: DataTypes.DATE,
      allowNull: true,
      validate: {
        isDate: {
          msg: 'Data de nascimento inválida'
        }
      }
    },
    cpf: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'CPF já cadastro em nossa base'
      }
    }
  }, {
    freezeTableName: true
  });
  Usuario.associate = function (models) {
    Usuario.hasMany(models.Sessao, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: false
      }
    });
    Usuario.hasOne(models.Imovelcomercial)
    Usuario.hasOne(models.Imovelresidencial)
  };
  return Usuario;
}
