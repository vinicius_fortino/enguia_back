module.exports = function (sequelize, DataTypes) {
  let Endereco = sequelize.define('Endereco', {
    logradouro: {
      type: DataTypes.STRING,
      allowNull: false
    },
    numero: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    complemento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    bairro: {
      type: DataTypes.STRING,
      allowNull: false
    },
    referencia: {
      type: DataTypes.STRING,
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
      freezeTableName: true
    });
  Endereco.associate = function (models) {
    Endereco.belongsTo(models.Cidade);
  }
  return Endereco;
};