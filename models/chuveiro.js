module.exports = function (sequelize, DataTypes) {
  let Chuveiro = sequelize.define('Chuveiro', {
    quantidade: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    potencia: {
      type: DataTypes.ENUM(['0-2400', '2401-3500', '3501-4600', '4601-5700', '5701-6800', '6801-7900', '7901-999999']),
      allowNull: false
    },
  }, {
    freezeTableName: true
  });

  Chuveiro.associate = function (models) {
    Chuveiro.belongsTo(models.Ambiente)
  }
  return Chuveiro;
};
