module.exports = function (sequelize, DataTypes) {
  let Outros = sequelize.define('Outros', {
    tipo: {
      type: DataTypes.ENUM(['Ferro-Eletrico', 'Forno-Microondas', 'Maquina-de-Lavar', 'Microcomputador', 'Fogao-Eletrico', 'Ventilador']),
      allowNull: false
    },
    potencia: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    quantidade: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    tempomedio: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    freezeTableName: true
  });

  Outros.associate = function (models) {
    Outros.belongsTo(models.Ambiente)
  }
  return Outros;
};
