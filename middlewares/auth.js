const jwt = require('jsonwebtoken');
const jwtsecret = require('../config/secret')[process.env.NODE_ENV];
const db = require('../models/index');

module.exports = async function (req, res, next) {

  try {
    let tokensessao;
    if (req.headers.authorization) {
      tokensessao = req.headers.authorization ? req.headers.authorization.split(' ')[1] : req.cookies.authorization;
    }

    if (tokensessao) {
      jwt.verify(tokensessao, jwtsecret);

      let sessao = await db.Sessao.findOne({
        where: {
          tokensessao: tokensessao
        },
        include: [{
          model: db.Usuario,
          required: true
        }]
      });

      if (sessao && sessao.Usuario)
        req.sessao = sessao;
    }

    req.sessaoNaoAutorizada = new Error('Não autorizado');
    req.sessaoNaoAutorizada.status = 401;

    next();

  } catch (err) {
    res.status(400).send({
      error: 'Erro interno',
      message: err.message
    })
  }
};